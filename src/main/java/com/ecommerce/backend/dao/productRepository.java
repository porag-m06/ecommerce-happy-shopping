package com.ecommerce.backend.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import com.ecommerce.backend.entity.Product;

public interface productRepository extends JpaRepository<Product, Long> {

}
