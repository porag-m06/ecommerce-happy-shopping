//package com.ecommerce.backend.config;
//
//import org.springframework.context.annotation.Configuration;
//import org.springframework.data.rest.core.config.RepositoryRestConfiguration;
//import org.springframework.data.rest.webmvc.config.RepositoryRestConfigurer;
//import org.springframework.http.HttpMethod;
//import org.springframework.web.servlet.config.annotation.CorsRegistry;
//
//import com.ecommerce.backend.entity.Product;
//import com.ecommerce.backend.entity.ProductCategory;
//
//
//@Configuration
//public class MyDataRestConfig  implements RepositoryRestConfigurer{
//
//	@Override
//	public void configureRepositoryRestConfiguration(RepositoryRestConfiguration config, CorsRegistry cors) {
//		HttpMethod [ ] theUnsupportedActions = {HttpMethod.POST, HttpMethod.DELETE, HttpMethod.PUT};
//		
//		//disabling the actions for Products
//		config.getExposureConfiguration().forDomainType(Product.class)
//		.withItemExposure((metdata, httpMethods) -> httpMethods.disable(theUnsupportedActions) )
//		.withCollectionExposure((metdata, httpMethods) -> httpMethods.disable(theUnsupportedActions));
//
//		//disabling the actions for ProductCategory
//				config.getExposureConfiguration().forDomainType(ProductCategory.class)
//				.withItemExposure((metdata, httpMethods) -> httpMethods.disable(theUnsupportedActions) )
//				.withCollectionExposure((metdata, httpMethods) -> httpMethods.disable(theUnsupportedActions));
//
//	
//	
//	}
//	
//
//}
